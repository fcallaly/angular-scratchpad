import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  public title = 'stock-price';
  private clickCount = 0;
  public imgUrl: string;

  constructor() {
    console.log('this happens when the component is created');
  }

  ngOnInit() {
    //this.clickCount = "hello";
    console.log('this happens when angular is fully loaded');
  }

  clickHandler() {
    console.log("clickHandler happened");
    this.title = 'new click ' + this.clickCount++;
  }
}
