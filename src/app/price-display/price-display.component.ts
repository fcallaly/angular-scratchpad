import { Component, OnInit } from '@angular/core';
import { PriceServiceService } from '../price-service.service';

@Component({
  selector: 'app-price-display',
  templateUrl: './price-display.component.html',
  styleUrls: ['./price-display.component.css']
})
export class PriceDisplayComponent implements OnInit {

  public priceData: any
  public ticker: string
  public chartData: any

  // chart options
  // options
  legend: boolean = true;
  showLabels: boolean = true;
  animations: boolean = true;
  xAxis: boolean = true;
  yAxis: boolean = true;
  showYAxisLabel: boolean = true;
  showXAxisLabel: boolean = true;
  xAxisLabel: string = 'Date';
  yAxisLabel: string = 'USD';
  timeline: boolean = true;

  constructor(private priceService: PriceServiceService) { }

  ngOnInit(): void {
    this.priceService.findAll().subscribe( priceData => {
      console.log("Recieved Price Data:")
      console.log(priceData)
      this.ticker = priceData.ticker
      this.priceData = priceData.price_data
      this.createChartData()
    },
    error => {
      console.log("Error getting price data:")
      console.log(error)
    })
  }

  createChartData() {
    this.chartData = {
      "name": this.ticker,
      "series": []
    }

    for(let price of this.priceData) {
      this.chartData.series.push({"name": price[0],
                                  "value": price[1]})
    }

    console.log(this.chartData)
    this.chartData = [this.chartData]
  }
}
