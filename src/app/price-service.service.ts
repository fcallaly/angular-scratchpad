import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PriceServiceService {

  constructor(private httpClient: HttpClient) { }

  public findAll(): Observable<any> {
    return this.httpClient.get<any>(
              environment.priceUrl + '?ticker=AMZN&num_days=5');
  }

  /*
  save(employee: any): Observable<any> { 

    return this.httpClient.post<any>( 
      environment.backendUrl,
      employee, 
      {headers : new HttpHeaders({ 'Content-Type': 'application/json' })}); 
    }*/ 
}
